@extends('layout.master')
@section('title')
    Halaman Bio Cast
@endsection
@section('subtitle')
    Bio Cast
@endsection
@section('content')

<h1 class="text-primary">{{$cast->nama}}</h1>
<p>{{$cast->bio}}</p>

@endsection