@extends('layout.master')
@section('title')
    Halaman Tambah Cast
@endsection
@section('subtitle')
    Tambah Cast
@endsection
@section('content')
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
            <label for="title">Nama</label>
            <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Umur</label>
            <input type="text" class="form-control" name="umur" placeholder="Masukkan Umur">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Bio</label>
            <textarea name="bio" class="form-control" cols="30" rows="10"></textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>

@endsection
