@extends('layout.master')
@section('title')
    Halaman Biodata
@endsection
@section('subtitle')
    Halaman Biodata
@endsection
@section('content')
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <label>First Name:</label> <br> <br>
        <input type="text" name="fname"> <br> <br>
        <label>Last Name:</label> <br> <br>
        <input type="text" name="lname"> <br> <br>
        <label>Gender:</label> <br> <br>
        <input type="radio" name="gender"> Male <br>
        <input type="radio" name="gender"> Female <br>
        <input type="radio" name="gender"> Other <br> <br>
        <label>Nationality:</label> <br> <br>
        <select name="nationality">
            <option value="">Indonesian</option>
            <option value="">Malaysian</option>
            <option value="">Other</option>
        </select> <br> <br>
        <label>Language Spoken:</label> <br> <br>
        <input type="checkbox" name="ls"> Bahasa Indonesia <br>
        <input type="checkbox" name="ls"> English <br>
        <input type="checkbox" name="ls"> Other <br> <br>
        <label>Bio:</label> <br> <br>
        <textarea cols="30" rows="10"></textarea> <br>

        <input type="submit" value="Sign Up">
    </form>
@endsection
