@extends('layout.master')
@section('title')
    Halaman Welcome
@endsection
@section('subtitle')
    Halaman Welcome
@endsection
@section('content')
    <h1>SELAMAT DATANG {{ $namaDepan }} {{ $namaBelakang }}</h1>
    <h2>Terima kasih telah bergabung di SanberBook. Social Media kita bersama!</h2>
@endsection
